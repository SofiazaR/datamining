import vk
import re
import string
import emoji
import pandas as pd
import matplotlib.pyplot as plt
import psycopg2
from psycopg2 import sql
import configparser

config = configparser.ConfigParser()
config.read('security.ini', encoding='UTF-8-SIG')


def visual_show(words):
    df = pd.DataFrame.from_dict(words, orient='index', columns=['count'])
    df = df.sort_values(by=['count']).tail(100)
    ax = df.plot(kind='bar', title='words', figsize=(15, 10), legend=True, fontsize=11)
    ax.set_xlabel('words', fontsize=12)
    ax.set_ylabel('counts', fontsize=12)
    plt.savefig('visual_words.png')


def save_words(words):
    connect = psycopg2.connect(dbname=config.get('psql', 'dbname'), user=config.get('psql', 'user'),
                               password=config.get('psql', 'password'), host=config.get('psql', 'host'))
    cursor = connect.cursor()
    connect.autocommit = True
    cursor.execute('truncate words')
    s_words = []
    for i in words.items():
        s_words.append(i)
    stmt = sql.SQL('insert into words(word,count) values {}').format(sql.SQL(",").join(map(sql.Literal, s_words)))
    cursor.execute(stmt)


def clear_text(post):
    text = re.sub(r'^https?:\/\/.*[\r\n]*', '', post['text'], flags=re.MULTILINE)
    text = text.translate(str.maketrans(dict.fromkeys(string.punctuation)))
    text = text.translate(str.maketrans(dict.fromkeys(b'1234567890')))
    text = re.sub(emoji.get_emoji_regexp(), r'', text)

    return text


session = vk.Session(access_token=config.get('vk', 'token'))
api = vk.API(session=session, v='5.85')
response = api.wall.get(domain='itis_kfu', count=100)['items'] + \
           api.wall.get(domain='itis_kfu', offset=100, count=100)['items']
words = {}

for post in response:
    for i in clear_text(post).split():
        words[i] = words.get(i, 0) + 1

visual_show(words)
save_words(words)
