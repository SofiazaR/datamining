from datetime import timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),

}
dag = DAG(
    'collect_dag',
    default_args=default_args,
    description='Parser DAG',
    schedule_interval='@weekly',
    start_date=days_ago(2),
    tags=['vk_api'],
)

BashOperator(
    task_id='main_task',
    bash_command='cd /opt/datamining/Vkparser;'
                 '. venv/bin/activate;'
                 'python3 /Vkparser/main.py',
    dag=dag)
